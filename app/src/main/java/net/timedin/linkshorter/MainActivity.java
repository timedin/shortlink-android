package net.timedin.linkshorter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.timedin.jUtils.methods.Curl;
import net.timedin.linkshorter.enums.Settings;
import net.timedin.linkshorter.helper.SettingsManager;

import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

public class MainActivity extends Activity {
    private final MainActivity instance = this;
    private EditText text;
    private TextView l_error;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        text = findViewById(R.id.url_out);
        Button btn_copy = findViewById(R.id.btn_copy);
        Button btn_settings = findViewById(R.id.btn_settings);
        l_error = findViewById(R.id.l_error);
        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        btn_copy.setOnClickListener(view -> {
            ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Shortened URL", text.getText());
            clipboard.setPrimaryClip(clip);
        });
        btn_settings.setOnClickListener(view -> {
            Intent settings = new Intent(this, SettingsActivity.class);
            startActivity(settings);
        });

        if (Intent.ACTION_SEND.equals(action) && type != null) {
            if ("text/plain".equals(type)) {
                text.setText(R.string.loading);
                String content = intent.getStringExtra(Intent.EXTRA_TEXT);
                CompletableFuture.runAsync(() -> handleURL(content));
            }
        } else {
            text.setText("");
        }
    }
    private void handleURL(String in) {
        HashMap<String, Object> options = new HashMap<>();
        options.put("protected", false);
        options.put("tiny", true);
        options.put("url", in);
        options.put("usages", 0);
        try {
            URL api = new URL(SettingsManager.getSetting(this, Settings.api_uri));
            Curl curl = new Curl(api, Curl.Method.POST, JSONObject.toJSONString(options));
            Map<String, Object> resp = curl.getJSONResponse();

            instance.runOnUiThread(() -> {
                if (Objects.equals(resp.get("state"), "success")) {
                    text.setText(String.format("%s%s", SettingsManager.getSetting(this, Settings.short_uri), resp.get("link_ID")));
                } else {
                    text.setText(getString(R.string.error, JSONObject.toJSONString(resp)));
                }
            });
        } catch (MalformedURLException e) {
            instance.runOnUiThread(() -> l_error.setText(getString(R.string.error_invalid_api_uri)));
        } catch (IOException e) {
            instance.runOnUiThread(() -> l_error.setText(Objects.requireNonNull(e.getCause()).getLocalizedMessage()));
        } catch (ParseException e) {
            instance.runOnUiThread(() -> l_error.setText(getString(R.string.error_invalid_response)));
        }
    }
}