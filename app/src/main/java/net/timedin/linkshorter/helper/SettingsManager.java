package net.timedin.linkshorter.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import net.timedin.linkshorter.enums.Settings;

public class SettingsManager {
    static SharedPreferences getSharedPreferences(Context ctx) {
        return PreferenceManager.getDefaultSharedPreferences(ctx);
    }

    public static void setData(Context ctx, Settings name, String value) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.putString(name.getName(), value);
        editor.apply();
    }

    public static String getDefault(Context ctx, Settings name) {
        return ctx.getString(name.getIdentifier());
    }

    public static void deleteSetting(Context ctx, Settings name) {
        SharedPreferences.Editor editor = getSharedPreferences(ctx).edit();
        editor.remove(name.getName());
        editor.apply();
    }

    public static String getSetting(Context ctx, Settings name) {
        return getSharedPreferences(ctx).getString(name.getName(), ctx.getString(name.getIdentifier()));
    }

    public static void resetSetting(Context ctx, Settings name) {
        setData(ctx, name, getDefault(ctx, name));
    }
}