package net.timedin.linkshorter.enums;

import net.timedin.linkshorter.R;

public enum Settings {
    api_uri("api_uri", R.string.default_api_url),short_uri("short_uri",R.string.default_short_url);
    private final int identifier;
    private final String name;
    Settings(String name, Integer identifier) {
        this.name = name;
        this.identifier = identifier;
    }
    public String getName() {
        return name;
    }

    public int getIdentifier() {
        return identifier;
    }
}
