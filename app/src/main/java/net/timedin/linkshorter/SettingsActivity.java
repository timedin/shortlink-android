package net.timedin.linkshorter;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import net.timedin.linkshorter.enums.Settings;
import net.timedin.linkshorter.helper.SettingsManager;

import java.net.MalformedURLException;
import java.net.URL;

public class SettingsActivity extends Activity {
    private EditText tf_api_uri;
    private EditText tf_short_url;
    private TextView l_error;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        tf_short_url = findViewById(R.id.tf_short_url);
        tf_api_uri = findViewById(R.id.tf_api_uri);
        Button btn_save = findViewById(R.id.btn_save_exit);
        l_error = findViewById(R.id.l_error_rsp);
        Button btn_reset_settings = findViewById(R.id.btn_reset_setting);

        btn_save.setOnClickListener(view -> {
            try {
                URL shortURL = new URL(tf_api_uri.getText().toString());
                SettingsManager.setData(this, Settings.api_uri, shortURL.toString());
            } catch (MalformedURLException e) {
                l_error.setText(getString(R.string.error_invalid_api_uri));
                return;
            }
            try {
                URL apiURL = new URL(tf_short_url.getText().toString());
                SettingsManager.setData(this, Settings.short_uri, apiURL.toString());
            } catch (MalformedURLException e) {
                l_error.setText(getString(R.string.error_invalid_short_uri));
                return;
            }
            finish();
        });
        tf_api_uri.setOnEditorActionListener((textView, i,keyEvent)->{
            l_error.setText("");
            return false;
        });
        btn_reset_settings.setOnClickListener(view ->{
            SettingsManager.resetSetting(this, Settings.api_uri);
            SettingsManager.setData(this, Settings.short_uri, getString(R.string.default_short_url));
            initFields();
        });
        initFields();
    }
    private void initFields() {
        tf_api_uri.setText(SettingsManager.getSetting(this, Settings.api_uri));
        tf_short_url.setText(SettingsManager.getSetting(this, Settings.short_uri));
    }
}
